const { Client } = require('discord.js');
const client = new Client();
require('dotenv').config();

client.on('ready', () => {
    console.log(`Bot is ready as ${client.user.tag}`);
    client.user.setStatus('dnd');
    console.log(client.user.presence.status);
});

client.on('message', message => {
    //Receiving the message
    console.log(message.content);
    if(message.content === 'ping') {
        message.reply('pong');
    }

    if(message.content === 'hello') {
        message.channel.send(`hello ${message.author}!`);
    }

    if(message.content.includes('!test')) {
        message.channel.send(`Glad your are testing`);
    }

    if(message.content.includes('!sherl')) {
        message.channel.send('http://twitter.com/rodol_28');
    }
});


client.login(process.env.TOKEN_DISCORD);